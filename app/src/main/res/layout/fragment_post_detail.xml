<!--
  ~ Copyright 2018 Google LLC
  ~
  ~ Licensed under the Apache License, Version 2.0 (the "License");
  ~ you may not use this file except in compliance with the License.
  ~ You may obtain a copy of the License at
  ~
  ~     https://www.apache.org/licenses/LICENSE-2.0
  ~
  ~ Unless required by applicable law or agreed to in writing, software
  ~ distributed under the License is distributed on an "AS IS" BASIS,
  ~ WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  ~ See the License for the specific language governing permissions and
  ~ limitations under the License.
  -->

<layout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools">

    <data>
        <import type="com.google.samples.apps.sunflower.data.Post"/>
        <variable
            name="viewModel"
            type="com.google.samples.apps.sunflower.viewmodels.PostDetailViewModel" />
        <variable
            name="callback"
            type="com.google.samples.apps.sunflower.PostDetailFragment.Callback" />
    </data>

    <androidx.coordinatorlayout.widget.CoordinatorLayout
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        android:fitsSystemWindows="true"
        android:background="?attr/colorSurface"
        tools:context="com.google.samples.apps.sunflower.GardenActivity"
        tools:ignore="MergeRootFrame">

        <com.google.android.material.appbar.AppBarLayout
            android:id="@+id/appbar"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:animateLayoutChanges="true"
            android:background="?attr/colorSurface"
            android:fitsSystemWindows="true"
            android:stateListAnimator="@animator/show_toolbar"
            android:theme="@style/ThemeOverlay.AppCompat.Dark.ActionBar">

            <com.google.android.material.appbar.CollapsingToolbarLayout
                android:id="@+id/toolbar_layout"
                android:layout_width="match_parent"
                android:layout_height="match_parent"
                android:fitsSystemWindows="true"
                app:collapsedTitleGravity="center"
                app:collapsedTitleTextAppearance="@style/TextAppearance.Sunflower.Toolbar.Text"
                app:contentScrim="?attr/colorSurface"
                app:layout_scrollFlags="scroll|exitUntilCollapsed"
                app:statusBarScrim="?attr/colorSurface"
                app:title="@{viewModel.post.title}"
                app:titleEnabled="false"
                app:toolbarId="@id/toolbar">

                <androidx.appcompat.widget.Toolbar
                    android:id="@+id/toolbar"
                    android:layout_width="match_parent"
                    android:layout_height="?attr/actionBarSize"
                    android:background="@android:color/transparent"
                    app:contentInsetStartWithNavigation="0dp"
                    app:layout_collapseMode="pin"
                    app:menu="@menu/menu_plant_detail"
                    app:navigationIcon="@drawable/ic_detail_back"
                    app:titleTextColor="?attr/colorOnSurface" />

            </com.google.android.material.appbar.CollapsingToolbarLayout>

        </com.google.android.material.appbar.AppBarLayout>

        <androidx.core.widget.NestedScrollView
            android:id="@+id/post_detail_scrollview"
            android:layout_width="match_parent"
            android:layout_height="match_parent"
            android:clipToPadding="false"
            android:paddingBottom="@dimen/fab_bottom_padding"
            app:layout_behavior="@string/appbar_scrolling_view_behavior">

            <androidx.constraintlayout.widget.ConstraintLayout
                android:layout_width="match_parent"
                android:layout_height="match_parent"
                android:layout_margin="@dimen/margin_normal">

                <TextView
                    android:id="@+id/plant_post_name"
                    android:layout_width="0dp"
                    android:layout_height="wrap_content"
                    android:layout_marginStart="@dimen/margin_small"
                    android:layout_marginEnd="@dimen/margin_small"
                    android:clickable="true"
                    android:focusable="true"
                    android:text="@{viewModel.post.title}"
                    android:textAppearance="?attr/textAppearanceHeadline5"
                    app:layout_constraintEnd_toEndOf="parent"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintTop_toTopOf="parent"
                    tools:text="AppleAppleAppleAppleAppleAppleAppleAppleApple" />

                <TextView
                    android:id="@+id/plant_watering_header"
                    android:layout_width="0dp"
                    android:layout_height="wrap_content"
                    android:layout_marginStart="@dimen/margin_small"
                    android:layout_marginTop="@dimen/margin_normal"
                    android:layout_marginEnd="@dimen/margin_small"
                    android:text="@string/watering_needs_prefix"
                    android:textColor="?attr/colorAccent"
                    android:textStyle="bold"
                    app:layout_constraintEnd_toEndOf="parent"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintTop_toBottomOf="@id/plant_post_name" />

                <TextView
                    android:id="@+id/plant_description"
                    style="?android:attr/textAppearanceMedium"
                    android:layout_width="0dp"
                    android:layout_height="wrap_content"
                    android:layout_marginStart="@dimen/margin_small"
                    android:layout_marginEnd="@dimen/margin_small"
                    android:minHeight="@dimen/plant_description_min_height"
                    android:textAppearance="@style/TextAppearance.AppCompat.Body1"
                    android:textColor="@color/design_default_color_on_secondary"
                    android:textIsSelectable="true"
                    app:layout_constraintEnd_toEndOf="parent"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintTop_toBottomOf="@+id/plant_watering_header"
                    app:renderHtml="@{viewModel.post.content}"
                    tools:text="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. " />

            </androidx.constraintlayout.widget.ConstraintLayout>

        </androidx.core.widget.NestedScrollView>

        <com.google.android.material.floatingactionbutton.FloatingActionButton
            android:id="@+id/fab"
            style="@style/Widget.MaterialComponents.FloatingActionButton"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:layout_margin="@dimen/fab_margin"
            android:onClick="@{() -> callback.add(viewModel.post)}"
            android:tint="@android:color/white"
            app:shapeAppearance="@style/ShapeAppearance.Sunflower.FAB"
            app:isFabGone="@{viewModel.isPlanted}"
            app:layout_anchor="@id/appbar"
            app:layout_anchorGravity="bottom|end"
            app:srcCompat="@drawable/ic_plus" />

    </androidx.coordinatorlayout.widget.CoordinatorLayout>

</layout>
