/*
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.google.samples.apps.sunflower.data

import androidx.room.*
import java.time.ZonedDateTime

@Entity(
        tableName = "comments",
        foreignKeys = [
            ForeignKey(entity = Post::class, parentColumns = ["id"], childColumns = ["post_id"])
        ],
        indices = [Index("post_id")]
)
class Comment (
        @ColumnInfo(name = "post_id") val postId: String,
        @ColumnInfo(name = "created_at") val createdAt: ZonedDateTime = ZonedDateTime.now()
) {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    var commentId: Long = 0
}